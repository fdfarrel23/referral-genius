<?php

class Whatsapp_model extends MY_Model{

    function __construct(){
		parent::__construct();
	}

    function sendMessage($wa_number,$message){
        $curl = curl_init();

        curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://api.fonnte.com/send',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS => array(
        'target' => $wa_number,
        'message' => $message,
        ),
        CURLOPT_HTTPHEADER => array(
            'Authorization: '.$this->config->item('whatapp_token')
        ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);

    }
}

?>